all: install docker-start
	@echo "Project installed successfully! You can access Kadokadéo at http://kadokadeo.localhost/."

docker-start: docker-stop
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml up -d --no-recreate --remove-orphans

docker-watch:
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml up --no-recreate --remove-orphans

docker-stop:
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml stop

docker-remove: docker-stop
	docker rm kadokadeo_app kadokadeo_database kadokadeo_eternaltwin

bash-app:
	docker exec -u dev -it kadokadeo_app bash

bash-app-root:
	docker exec -it kadokadeo_app bash

bash-db:
	docker exec -it kadokadeo_database bash

bash-eternaltwin:
	docker exec -it kadokadeo_eternaltwin bash

reset-dependencies: install-app install-eternaltwin

build:
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml build
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u dev kadokadeo_app chown -R dev:dev /www
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node kadokadeo_eternaltwin chown -R node:node /www
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml up --no-start --remove-orphans

install: setup-env-variables build start-kadokadeo-database install-app
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node kadokadeo_eternaltwin yarn install
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node kadokadeo_eternaltwin yarn etwin db create

install-app:
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u dev kadokadeo_app composer install
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u dev kadokadeo_app composer run-script db:reset -- --no-confirm

install-eternaltwin:
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node kadokadeo_eternaltwin yarn install

reset-database:
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u dev kadokadeo_app composer run-script db:reset -- --no-confirm

reset-eternaltwin-database: 
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node kadokadeo_eternaltwin yarn etwin db create

start-kadokadeo-database:
	docker start kadokadeo_database

setup-env-variables:
	cp .env.docker .env
	cp eternaltwin/etwin.toml.example eternaltwin/etwin.toml

sync-database:
	docker compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u dev kadokadeo_app composer run-script db:sync -- --no-confirm