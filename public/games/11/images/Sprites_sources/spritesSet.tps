<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>6</int>
        <key>texturePackerVersion</key>
        <string>7.1.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPackMode</key>
        <enum type="SettingsBase::MultiPackMode">MultiPackOff</enum>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">sprites/arrow/1.png</key>
            <key type="filename">sprites/arrow/10.png</key>
            <key type="filename">sprites/arrow/11.png</key>
            <key type="filename">sprites/arrow/12.png</key>
            <key type="filename">sprites/arrow/13.png</key>
            <key type="filename">sprites/arrow/14.png</key>
            <key type="filename">sprites/arrow/15.png</key>
            <key type="filename">sprites/arrow/16.png</key>
            <key type="filename">sprites/arrow/17.png</key>
            <key type="filename">sprites/arrow/18.png</key>
            <key type="filename">sprites/arrow/19.png</key>
            <key type="filename">sprites/arrow/2.png</key>
            <key type="filename">sprites/arrow/20.png</key>
            <key type="filename">sprites/arrow/21.png</key>
            <key type="filename">sprites/arrow/22.png</key>
            <key type="filename">sprites/arrow/23.png</key>
            <key type="filename">sprites/arrow/24.png</key>
            <key type="filename">sprites/arrow/25.png</key>
            <key type="filename">sprites/arrow/3.png</key>
            <key type="filename">sprites/arrow/4.png</key>
            <key type="filename">sprites/arrow/5.png</key>
            <key type="filename">sprites/arrow/6.png</key>
            <key type="filename">sprites/arrow/7.png</key>
            <key type="filename">sprites/arrow/8.png</key>
            <key type="filename">sprites/arrow/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,10,17,20</rect>
                <key>scale9Paddings</key>
                <rect>9,10,17,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/bee/1.png</key>
            <key type="filename">sprites/bee/10.png</key>
            <key type="filename">sprites/bee/11.png</key>
            <key type="filename">sprites/bee/12.png</key>
            <key type="filename">sprites/bee/13.png</key>
            <key type="filename">sprites/bee/14.png</key>
            <key type="filename">sprites/bee/15.png</key>
            <key type="filename">sprites/bee/16.png</key>
            <key type="filename">sprites/bee/17.png</key>
            <key type="filename">sprites/bee/18.png</key>
            <key type="filename">sprites/bee/19.png</key>
            <key type="filename">sprites/bee/2.png</key>
            <key type="filename">sprites/bee/20.png</key>
            <key type="filename">sprites/bee/21.png</key>
            <key type="filename">sprites/bee/22.png</key>
            <key type="filename">sprites/bee/23.png</key>
            <key type="filename">sprites/bee/24.png</key>
            <key type="filename">sprites/bee/25.png</key>
            <key type="filename">sprites/bee/26.png</key>
            <key type="filename">sprites/bee/3.png</key>
            <key type="filename">sprites/bee/4.png</key>
            <key type="filename">sprites/bee/5.png</key>
            <key type="filename">sprites/bee/6.png</key>
            <key type="filename">sprites/bee/7.png</key>
            <key type="filename">sprites/bee/8.png</key>
            <key type="filename">sprites/bee/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.571429,0.69697</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,17</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/bird/1.png</key>
            <key type="filename">sprites/bird/10.png</key>
            <key type="filename">sprites/bird/11.png</key>
            <key type="filename">sprites/bird/12.png</key>
            <key type="filename">sprites/bird/13.png</key>
            <key type="filename">sprites/bird/14.png</key>
            <key type="filename">sprites/bird/15.png</key>
            <key type="filename">sprites/bird/16.png</key>
            <key type="filename">sprites/bird/17.png</key>
            <key type="filename">sprites/bird/18.png</key>
            <key type="filename">sprites/bird/2.png</key>
            <key type="filename">sprites/bird/3.png</key>
            <key type="filename">sprites/bird/4.png</key>
            <key type="filename">sprites/bird/5.png</key>
            <key type="filename">sprites/bird/6.png</key>
            <key type="filename">sprites/bird/7.png</key>
            <key type="filename">sprites/bird/8.png</key>
            <key type="filename">sprites/bird/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.380952,0.588889</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,23,42,45</rect>
                <key>scale9Paddings</key>
                <rect>21,23,42,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/boarlet/1.png</key>
            <key type="filename">sprites/boarlet/2.png</key>
            <key type="filename">sprites/boarlet/3.png</key>
            <key type="filename">sprites/boarlet/4.png</key>
            <key type="filename">sprites/boarlet/5.png</key>
            <key type="filename">sprites/boarlet/6.png</key>
            <key type="filename">sprites/boarlet/7.png</key>
            <key type="filename">sprites/boarlet/8.png</key>
            <key type="filename">sprites/boarlet/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.534483,0.882353</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,9,29,17</rect>
                <key>scale9Paddings</key>
                <rect>15,9,29,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/bonus/background/1.png</key>
            <key type="filename">sprites/bonus/background/2.png</key>
            <key type="filename">sprites/bonus/background/3.png</key>
            <key type="filename">sprites/bonus/reflect/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,14,14</rect>
                <key>scale9Paddings</key>
                <rect>7,7,14,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/bonusPicked/1.png</key>
            <key type="filename">sprites/bonusPicked/10.png</key>
            <key type="filename">sprites/bonusPicked/11.png</key>
            <key type="filename">sprites/bonusPicked/12.png</key>
            <key type="filename">sprites/bonusPicked/13.png</key>
            <key type="filename">sprites/bonusPicked/14.png</key>
            <key type="filename">sprites/bonusPicked/15.png</key>
            <key type="filename">sprites/bonusPicked/16.png</key>
            <key type="filename">sprites/bonusPicked/17.png</key>
            <key type="filename">sprites/bonusPicked/18.png</key>
            <key type="filename">sprites/bonusPicked/19.png</key>
            <key type="filename">sprites/bonusPicked/2.png</key>
            <key type="filename">sprites/bonusPicked/20.png</key>
            <key type="filename">sprites/bonusPicked/21.png</key>
            <key type="filename">sprites/bonusPicked/22.png</key>
            <key type="filename">sprites/bonusPicked/23.png</key>
            <key type="filename">sprites/bonusPicked/3.png</key>
            <key type="filename">sprites/bonusPicked/4.png</key>
            <key type="filename">sprites/bonusPicked/5.png</key>
            <key type="filename">sprites/bonusPicked/6.png</key>
            <key type="filename">sprites/bonusPicked/7.png</key>
            <key type="filename">sprites/bonusPicked/8.png</key>
            <key type="filename">sprites/bonusPicked/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,29,57,57</rect>
                <key>scale9Paddings</key>
                <rect>29,29,57,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/deathSmoke/1.png</key>
            <key type="filename">sprites/deathSmoke/10.png</key>
            <key type="filename">sprites/deathSmoke/11.png</key>
            <key type="filename">sprites/deathSmoke/12.png</key>
            <key type="filename">sprites/deathSmoke/13.png</key>
            <key type="filename">sprites/deathSmoke/14.png</key>
            <key type="filename">sprites/deathSmoke/15.png</key>
            <key type="filename">sprites/deathSmoke/2.png</key>
            <key type="filename">sprites/deathSmoke/3.png</key>
            <key type="filename">sprites/deathSmoke/4.png</key>
            <key type="filename">sprites/deathSmoke/5.png</key>
            <key type="filename">sprites/deathSmoke/6.png</key>
            <key type="filename">sprites/deathSmoke/7.png</key>
            <key type="filename">sprites/deathSmoke/8.png</key>
            <key type="filename">sprites/deathSmoke/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.561404,0.694915</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,15,29,29</rect>
                <key>scale9Paddings</key>
                <rect>14,15,29,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/ennemyAlert/1.png</key>
            <key type="filename">sprites/ennemyAlert/10.png</key>
            <key type="filename">sprites/ennemyAlert/11.png</key>
            <key type="filename">sprites/ennemyAlert/12.png</key>
            <key type="filename">sprites/ennemyAlert/13.png</key>
            <key type="filename">sprites/ennemyAlert/14.png</key>
            <key type="filename">sprites/ennemyAlert/15.png</key>
            <key type="filename">sprites/ennemyAlert/16.png</key>
            <key type="filename">sprites/ennemyAlert/17.png</key>
            <key type="filename">sprites/ennemyAlert/18.png</key>
            <key type="filename">sprites/ennemyAlert/19.png</key>
            <key type="filename">sprites/ennemyAlert/2.png</key>
            <key type="filename">sprites/ennemyAlert/20.png</key>
            <key type="filename">sprites/ennemyAlert/21.png</key>
            <key type="filename">sprites/ennemyAlert/22.png</key>
            <key type="filename">sprites/ennemyAlert/23.png</key>
            <key type="filename">sprites/ennemyAlert/24.png</key>
            <key type="filename">sprites/ennemyAlert/25.png</key>
            <key type="filename">sprites/ennemyAlert/26.png</key>
            <key type="filename">sprites/ennemyAlert/27.png</key>
            <key type="filename">sprites/ennemyAlert/28.png</key>
            <key type="filename">sprites/ennemyAlert/29.png</key>
            <key type="filename">sprites/ennemyAlert/3.png</key>
            <key type="filename">sprites/ennemyAlert/30.png</key>
            <key type="filename">sprites/ennemyAlert/31.png</key>
            <key type="filename">sprites/ennemyAlert/32.png</key>
            <key type="filename">sprites/ennemyAlert/33.png</key>
            <key type="filename">sprites/ennemyAlert/34.png</key>
            <key type="filename">sprites/ennemyAlert/35.png</key>
            <key type="filename">sprites/ennemyAlert/36.png</key>
            <key type="filename">sprites/ennemyAlert/37.png</key>
            <key type="filename">sprites/ennemyAlert/38.png</key>
            <key type="filename">sprites/ennemyAlert/39.png</key>
            <key type="filename">sprites/ennemyAlert/4.png</key>
            <key type="filename">sprites/ennemyAlert/40.png</key>
            <key type="filename">sprites/ennemyAlert/41.png</key>
            <key type="filename">sprites/ennemyAlert/42.png</key>
            <key type="filename">sprites/ennemyAlert/43.png</key>
            <key type="filename">sprites/ennemyAlert/44.png</key>
            <key type="filename">sprites/ennemyAlert/45.png</key>
            <key type="filename">sprites/ennemyAlert/5.png</key>
            <key type="filename">sprites/ennemyAlert/6.png</key>
            <key type="filename">sprites/ennemyAlert/7.png</key>
            <key type="filename">sprites/ennemyAlert/8.png</key>
            <key type="filename">sprites/ennemyAlert/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>1,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,8,9,17</rect>
                <key>scale9Paddings</key>
                <rect>4,8,9,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/feather/animated/1.png</key>
            <key type="filename">sprites/feather/animated/10.png</key>
            <key type="filename">sprites/feather/animated/11.png</key>
            <key type="filename">sprites/feather/animated/12.png</key>
            <key type="filename">sprites/feather/animated/13.png</key>
            <key type="filename">sprites/feather/animated/14.png</key>
            <key type="filename">sprites/feather/animated/15.png</key>
            <key type="filename">sprites/feather/animated/16.png</key>
            <key type="filename">sprites/feather/animated/17.png</key>
            <key type="filename">sprites/feather/animated/18.png</key>
            <key type="filename">sprites/feather/animated/19.png</key>
            <key type="filename">sprites/feather/animated/2.png</key>
            <key type="filename">sprites/feather/animated/20.png</key>
            <key type="filename">sprites/feather/animated/21.png</key>
            <key type="filename">sprites/feather/animated/22.png</key>
            <key type="filename">sprites/feather/animated/23.png</key>
            <key type="filename">sprites/feather/animated/24.png</key>
            <key type="filename">sprites/feather/animated/25.png</key>
            <key type="filename">sprites/feather/animated/26.png</key>
            <key type="filename">sprites/feather/animated/27.png</key>
            <key type="filename">sprites/feather/animated/28.png</key>
            <key type="filename">sprites/feather/animated/29.png</key>
            <key type="filename">sprites/feather/animated/3.png</key>
            <key type="filename">sprites/feather/animated/30.png</key>
            <key type="filename">sprites/feather/animated/31.png</key>
            <key type="filename">sprites/feather/animated/32.png</key>
            <key type="filename">sprites/feather/animated/33.png</key>
            <key type="filename">sprites/feather/animated/34.png</key>
            <key type="filename">sprites/feather/animated/35.png</key>
            <key type="filename">sprites/feather/animated/36.png</key>
            <key type="filename">sprites/feather/animated/37.png</key>
            <key type="filename">sprites/feather/animated/38.png</key>
            <key type="filename">sprites/feather/animated/39.png</key>
            <key type="filename">sprites/feather/animated/4.png</key>
            <key type="filename">sprites/feather/animated/40.png</key>
            <key type="filename">sprites/feather/animated/41.png</key>
            <key type="filename">sprites/feather/animated/5.png</key>
            <key type="filename">sprites/feather/animated/6.png</key>
            <key type="filename">sprites/feather/animated/7.png</key>
            <key type="filename">sprites/feather/animated/8.png</key>
            <key type="filename">sprites/feather/animated/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,8,29,15</rect>
                <key>scale9Paddings</key>
                <rect>15,8,29,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/feather/fixed/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.482759,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,2,15,3</rect>
                <key>scale9Paddings</key>
                <rect>7,2,15,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/hero/braking/40.png</key>
            <key type="filename">sprites/hero/braking/41.png</key>
            <key type="filename">sprites/hero/braking/42.png</key>
            <key type="filename">sprites/hero/braking/43.png</key>
            <key type="filename">sprites/hero/braking/44.png</key>
            <key type="filename">sprites/hero/braking/45.png</key>
            <key type="filename">sprites/hero/braking/46.png</key>
            <key type="filename">sprites/hero/braking/47.png</key>
            <key type="filename">sprites/hero/braking/48.png</key>
            <key type="filename">sprites/hero/braking/49.png</key>
            <key type="filename">sprites/hero/braking/50.png</key>
            <key type="filename">sprites/hero/braking/51.png</key>
            <key type="filename">sprites/hero/braking/52.png</key>
            <key type="filename">sprites/hero/braking/53.png</key>
            <key type="filename">sprites/hero/braking/54.png</key>
            <key type="filename">sprites/hero/braking/55.png</key>
            <key type="filename">sprites/hero/braking/56.png</key>
            <key type="filename">sprites/hero/braking/57.png</key>
            <key type="filename">sprites/hero/breathing/1.png</key>
            <key type="filename">sprites/hero/breathing/10.png</key>
            <key type="filename">sprites/hero/breathing/11.png</key>
            <key type="filename">sprites/hero/breathing/12.png</key>
            <key type="filename">sprites/hero/breathing/13.png</key>
            <key type="filename">sprites/hero/breathing/14.png</key>
            <key type="filename">sprites/hero/breathing/15.png</key>
            <key type="filename">sprites/hero/breathing/16.png</key>
            <key type="filename">sprites/hero/breathing/17.png</key>
            <key type="filename">sprites/hero/breathing/18.png</key>
            <key type="filename">sprites/hero/breathing/19.png</key>
            <key type="filename">sprites/hero/breathing/2.png</key>
            <key type="filename">sprites/hero/breathing/20.png</key>
            <key type="filename">sprites/hero/breathing/21.png</key>
            <key type="filename">sprites/hero/breathing/22.png</key>
            <key type="filename">sprites/hero/breathing/23.png</key>
            <key type="filename">sprites/hero/breathing/24.png</key>
            <key type="filename">sprites/hero/breathing/25.png</key>
            <key type="filename">sprites/hero/breathing/26.png</key>
            <key type="filename">sprites/hero/breathing/27.png</key>
            <key type="filename">sprites/hero/breathing/28.png</key>
            <key type="filename">sprites/hero/breathing/3.png</key>
            <key type="filename">sprites/hero/breathing/4.png</key>
            <key type="filename">sprites/hero/breathing/5.png</key>
            <key type="filename">sprites/hero/breathing/6.png</key>
            <key type="filename">sprites/hero/breathing/7.png</key>
            <key type="filename">sprites/hero/breathing/8.png</key>
            <key type="filename">sprites/hero/breathing/9.png</key>
            <key type="filename">sprites/hero/endJumping/73.png</key>
            <key type="filename">sprites/hero/endJumping/74.png</key>
            <key type="filename">sprites/hero/endJumping/75.png</key>
            <key type="filename">sprites/hero/endJumping/76.png</key>
            <key type="filename">sprites/hero/endJumping/77.png</key>
            <key type="filename">sprites/hero/endJumping/78.png</key>
            <key type="filename">sprites/hero/endJumping/79.png</key>
            <key type="filename">sprites/hero/endJumping/80.png</key>
            <key type="filename">sprites/hero/endJumping/81.png</key>
            <key type="filename">sprites/hero/endJumping/82.png</key>
            <key type="filename">sprites/hero/endJumping/83.png</key>
            <key type="filename">sprites/hero/endJumping/84.png</key>
            <key type="filename">sprites/hero/endJumping/85.png</key>
            <key type="filename">sprites/hero/endJumping/86.png</key>
            <key type="filename">sprites/hero/endJumping/87.png</key>
            <key type="filename">sprites/hero/endJumping/88.png</key>
            <key type="filename">sprites/hero/endJumping/89.png</key>
            <key type="filename">sprites/hero/endJumping/90.png</key>
            <key type="filename">sprites/hero/endJumping/91.png</key>
            <key type="filename">sprites/hero/endJumping/92.png</key>
            <key type="filename">sprites/hero/endJumping/93.png</key>
            <key type="filename">sprites/hero/jumping/58.png</key>
            <key type="filename">sprites/hero/jumping/59.png</key>
            <key type="filename">sprites/hero/jumping/60.png</key>
            <key type="filename">sprites/hero/jumping/61.png</key>
            <key type="filename">sprites/hero/jumping/62.png</key>
            <key type="filename">sprites/hero/jumping/63.png</key>
            <key type="filename">sprites/hero/jumping/64.png</key>
            <key type="filename">sprites/hero/jumping/65.png</key>
            <key type="filename">sprites/hero/jumping/66.png</key>
            <key type="filename">sprites/hero/jumping/67.png</key>
            <key type="filename">sprites/hero/jumping/68.png</key>
            <key type="filename">sprites/hero/jumping/69.png</key>
            <key type="filename">sprites/hero/jumping/70.png</key>
            <key type="filename">sprites/hero/jumping/71.png</key>
            <key type="filename">sprites/hero/jumping/72.png</key>
            <key type="filename">sprites/hero/landing/100.png</key>
            <key type="filename">sprites/hero/landing/101.png</key>
            <key type="filename">sprites/hero/landing/102.png</key>
            <key type="filename">sprites/hero/landing/103.png</key>
            <key type="filename">sprites/hero/landing/104.png</key>
            <key type="filename">sprites/hero/landing/105.png</key>
            <key type="filename">sprites/hero/landing/94.png</key>
            <key type="filename">sprites/hero/landing/95.png</key>
            <key type="filename">sprites/hero/landing/96.png</key>
            <key type="filename">sprites/hero/landing/97.png</key>
            <key type="filename">sprites/hero/landing/98.png</key>
            <key type="filename">sprites/hero/landing/99.png</key>
            <key type="filename">sprites/hero/running/33.png</key>
            <key type="filename">sprites/hero/running/34.png</key>
            <key type="filename">sprites/hero/running/35.png</key>
            <key type="filename">sprites/hero/running/36.png</key>
            <key type="filename">sprites/hero/running/37.png</key>
            <key type="filename">sprites/hero/running/38.png</key>
            <key type="filename">sprites/hero/running/39.png</key>
            <key type="filename">sprites/hero/startRunning/29.png</key>
            <key type="filename">sprites/hero/startRunning/30.png</key>
            <key type="filename">sprites/hero/startRunning/31.png</key>
            <key type="filename">sprites/hero/startRunning/32.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.576923,0.865672</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,17,52,33</rect>
                <key>scale9Paddings</key>
                <rect>26,17,52,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/heroDead/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.679245,0.566038</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,27,27</rect>
                <key>scale9Paddings</key>
                <rect>13,13,27,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/jumpSmoke/1.png</key>
            <key type="filename">sprites/jumpSmoke/2.png</key>
            <key type="filename">sprites/jumpSmoke/3.png</key>
            <key type="filename">sprites/jumpSmoke/4.png</key>
            <key type="filename">sprites/jumpSmoke/5.png</key>
            <key type="filename">sprites/jumpSmoke/6.png</key>
            <key type="filename">sprites/jumpSmoke/7.png</key>
            <key type="filename">sprites/jumpSmoke/8.png</key>
            <key type="filename">sprites/jumpSmoke/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.591398,0.844828</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,15,47,29</rect>
                <key>scale9Paddings</key>
                <rect>23,15,47,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileLists</key>
        <map type="SpriteSheetMap">
            <key>default</key>
            <struct type="SpriteSheet">
                <key>files</key>
                <array>
                    <filename>sprites</filename>
                </array>
            </struct>
        </map>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
