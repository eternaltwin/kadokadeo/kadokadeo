CREATE TABLE "user_settings" (
    user_id USER_ID NOT NULL,
    email VARCHAR(255) NOT NULL,
    PRIMARY KEY (user_id)
);
